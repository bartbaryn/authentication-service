import { BadRequestException, Body, Controller, Post } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';

import { AuthorizeUserBody } from './input/authorizeUser.body';
import { AuthError, authFactory, verifyToken } from './models/authFactory';

const { JWT_SECRET } = process.env;

@Controller()
export class AuthController {
  @Post('/auth')
  authorizeUser(@Body() authorizeUserBody: AuthorizeUserBody): any {
    if (!JWT_SECRET) {
      throw new Error(
        'Missing JWT_SECRET env var. Set it and restart the server',
      );
    }

    const auth = authFactory(JWT_SECRET);
    const { username, password } = authorizeUserBody;

    try {
      const token = auth(username, password);
      return { token };
    } catch (error) {
      if (error instanceof AuthError) {
        throw new BadRequestException(error.message);
      }
    }
  }

  @MessagePattern({ cmd: 'verifyToken' })
  async verifyToken(data: { jwt: string }) {
    try {
      const res = verifyToken(data.jwt, JWT_SECRET);
      return res;
    } catch (error) {
      if (error instanceof AuthError) {
        throw new BadRequestException(error.message);
      }
    }
  }
}
