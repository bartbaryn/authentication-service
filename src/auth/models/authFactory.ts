import * as jwt from 'jsonwebtoken';

export type IUser = { userId: string; name: string; role: string };

const users = [
  {
    id: 123,
    role: 'basic',
    name: 'Basic Thomas',
    username: 'basic-thomas',
    password: 'sR-_pcoow-27-6PAwCD8',
  },
  {
    id: 434,
    role: 'premium',
    name: 'Premium Jim',
    username: 'premium-jim',
    password: 'GBLtTyq3E_UNjFnpo9m6',
  },
];

export class AuthError extends Error {}

export const authFactory = (secret: string) => (
  username: string,
  password: string,
): string => {
  const user = users.find((u) => u.username === username);

  if (!user || user.password !== password) {
    throw new AuthError('invalid username or password');
  }

  return jwt.sign(
    {
      userId: user.id,
      name: user.name,
      role: user.role,
    },
    secret,
    {
      issuer: 'https://www.netguru.com/',
      subject: `${user.id}`,
      expiresIn: 30 * 60,
    },
  );
};

export const verifyToken = (token: string, secret: string): IUser => {
  const decodedToken = jwt.verify(token, secret) as IUser;

  const user = users.find((u) => u.id === parseInt(decodedToken.userId, 10));
  if (!user) {
    throw new AuthError('User unauthorized');
  }

  return {
    userId: decodedToken.userId,
    name: decodedToken.name,
    role: decodedToken.role,
  };
};
