import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { Transport } from '@nestjs/microservices';

import { AppModule } from './app.module';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const port = 4000;

  app.connectMicroservice({
    transport: Transport.TCP,
    options: {
      host: 'localhost',
      port: 4100,
    },
  });

  await app.startAllMicroservicesAsync();
  await app.listen(port);
  Logger.log(`Service is listening on port: ${port}`, 'NestApplication');
}
bootstrap();
